task<JavaExec>("start-platform") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "jade.Boot"
    standardInput = System.`in`

    args = listOf(
        "-gui", "-services", "it.unibo.tucson.jade.service.TucsonService"
    )
}