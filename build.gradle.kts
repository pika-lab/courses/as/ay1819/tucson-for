//import de.undercouch.gradle.tasks.download.Download
//import jason.util.Config
//import jason.runtime.RunJasonProject


plugins {
    java
    //id("de.undercouch.download") version("3.4.3")
}


/*
buildscript {
    dependencies {
        classpath(files("libs/jason-2.4.jar"))
    }
}
*/

repositories {
    mavenCentral()
}


group = "it.unibo.as"
version = "1.0-SNAPSHOT"

fun projectFile(relative: String): File {
    return File(rootProject.projectDir, relative)
}

fun projectPath(relative: String): String {
    return projectFile(relative).absolutePath
}

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()

    dependencies {
        implementation(
            files(
                projectPath("libs/2p.jar"),
                projectPath("libs/tucson.jar")
            )
        )
        testImplementation("junit", "junit", "4.12")

        if (this@subprojects.name.contains("jade")) {
            implementation(
                files(
                    projectPath("libs/jade.jar"),
                    projectPath("libs/t4j-noexamples.jar")
                )
            )
        } else if (this@subprojects.name.contains("jason")) {
            implementation(
                files(
                    projectPath("libs/jason-2.4.jar"),
                    projectPath("libs/ant-1.10.5.jar"),
                    projectPath("libs/ant-launcher-1.10.5"),
                    projectPath("libs/t4jn.jar")
                )
            )
        }
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(
        files(
            projectPath("libs/2p.jar"),		
            projectPath("libs/tucson.jar")
        )
    )
}

task<JavaExec>("tucson") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.service.TucsonNodeService"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("inspector") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.introspection.tools.InspectorGUI"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("cli") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.service.tools.CommandLineInterpreter"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}
