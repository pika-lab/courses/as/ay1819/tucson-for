/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */
 
/* Agent 'bookSellerAgent' in project t4jn_bookTrading.mas2j */

/* Initial beliefs and rules */
// Could add the tuple centre as a configurable parameter

/* Initial goals */
!boot.

/* Plans */
+!boot
    <- .print("Started.");
       // list of books with alterbative prices
       for ( .member(X,
            [
                entry("harry_potter_and_the_sorcerer_stone", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_chamber_of_secrets", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_prisoner_of_azkaban", "8.79", "9.79", "10.79", "11.79", "12.79"),
                entry("harry_potter_and_the_goblet_of_fire", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_order_of_the_phoenix", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_half_blood_prince", "10.39", "11.39", "12.39", "13.39", "14.39"),
                entry("harry_potter_and_the_deathly_hallows", "10.19", "11.19", "12.19", "13.19", "14.19")
            ]
       )) {
            // for each book, draw a random price
            .random(Rand);
            t4jn.api.getArg(X, 0, Title); // seamless integration with Jason tuples
            t4jn.api.getArg(X, 1 + math.round(Rand * 4), PriceString);
            .print("Adding book ", Title, " with price ", PriceString, " to available books...");
            .term2string(Price, PriceString);
            +book(Title, Price);
       }
       !advertise.

+!advertise
    <- .my_name(Me);
       .print("Advertising 'book-trading' service to the 'default' tuple centre...");
       t4jn.api.out("default", "127.0.0.1", "20504",
            advertise(provider(Me), service(book_trading)), Out0
       );
       !wait_for_cfp.

+!wait_for_cfp
    <- .my_name(Me);
       .print("Waiting for CFP messages...");

       t4jn.api.in("default", "127.0.0.1", "20504",
            cfp(to(Me), from(B), book(T)), In0); // last parameter us the request unique (per-agent) ID
       t4jn.api.getResult(In0, Res0);

       t4jn.api.getArg(Res0, 1, From);  // from(B)
       t4jn.api.getArg(From, 0, Buyer); // B
       .print("Received CFP from ", Buyer);

       t4jn.api.getArg(Res0, 2, Book);  // book(T)
       t4jn.api.getArg(Book, 0, Title); // T
       .print("Checking availability of book ", Title, "...");

       !!send_proposal(Buyer, Title);
       !wait_for_cfp.

+!send_proposal(Buyer, Title) // if I have the book, propose its price
    <- .my_name(Me);

       .findall(book(X, Y), book(X, Y), Books);
       if ( .member(book(Title, Price), Books) ) {
            .print("Book ", Title, " available, proposing price ", Price, "...");
            t4jn.api.out("default", "127.0.0.1", "20504",
                proposal(to(Buyer), book(Title), from(Me), price(Price)), Out1);
            +buyer(Buyer);
            !handle_purchase;
       } else {
            .print("Book ", Title, " NOT available, informing client...");
            t4jn.api.out("default", "127.0.0.1", "20504",
                proposal(to(Buyer), book(Title), from(Me), price(unavailable)), Out2);
       }.

+!handle_purchase
    <- .my_name(Me);
       .print("Waiting for purchase orders...");
       t4jn.api.in("default", "127.0.0.1", "20504",
            order(X, from(B), to(Me), book(T)), In1);
       t4jn.api.getResult(In1, Res1);

       if (not (Res1 == null)) {
            t4jn.api.getArg(Res1, 0, Reply); // X

            t4jn.api.getArg(Res1, 1, FromB);  // from(B)
            t4jn.api.getArg(FromB, 0, Buyer); // B

            t4jn.api.getArg(Res1, 3, BookT);      // book(T)
            t4jn.api.getArg(BookT, 0, BookTitle); // T

            .findall(X, buyer(X), Buyers);
            if ( .member(Buyer, Buyers) ) {
                -buyer(Buyer);
                if (Reply == "accept") {
                    !!handle_delivery(Buyer, BookTitle);
                } else {
                    .print("Client ", Buyer, " rejected ", BookTitle);
                }
            } else {
                .print("Client ", Buyer, " is cheating!");
            }

       }.

@atomic +!handle_delivery(Buyer, BookTitle)
    <- .print("Received purchase order from ", Buyer);
       .findall(book(X, Y), book(X, Y), Books);
       if ( .member(book(BookTitle, Price), Books) ) {
            -book(BookTitle, Price);
            .print("Selling book ", BookTitle,
                " to agent ", Buyer, "...");
            t4jn.api.out("default", "127.0.0.1", "20504",
                purchase(confirm, to(Buyer), from(Me),
                book(BookTitle)), Out3);
       } else {
            .print("Sorry, book ", BookTitle,
                " is NOT available anymore :/");
            t4jn.api.out("default", "127.0.0.1", "20504",
                purchase(failure, to(Buyer), from(Me),
                book(BookTitle)), Out4);
       }.
