/*
 * Copyright 1999-2015 Alma Mater Studiorum - Universita' di Bologna
 *
 * This file is part of TuCSoN4Jason <http://tucson4jason.apice.unibo.it>.
 *
 *    TuCSoN4Jason is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published
 *    by the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    TuCSoN4Jason is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with TuCSoN4Jason.  If not, see
 *    <https://www.gnu.org/licenses/lgpl.html>.
 *
 */

!boot.

+!boot
    <- !!one;
       !two.

+!one
    <- .print("ONE START");
       t4jn.api.in("default", "127.0.0.1", "20504", hello(X,Y), Op0);
       .print("Getting result for op #", Op0);
       t4jn.api.getResult(Op0, Result);
       .print("Got: ", Result, " for op #", Op0);
       t4jn.api.out("default", "127.0.0.1", "20504", Result, Op1);
       .print("ONE END, got: ", Result, " for op #", Op1).

+!two
    <- .print("TW0 START");
       .print("action 1");
       .print("action 2");
       .print("action 3");
       t4jn.api.out("default", "127.0.0.1", "20504", hello(wonderful,world), Op2);
       .print("Op #", Op2, " done");
       .print("TWO END").
