import jason.util.Config
import jason.runtime.RunJasonProject

buildscript {
    dependencies {
        classpath(files("../libs/jason-2.4.jar"))
    }
}


fun projectFile(relative: String): File {
    return File(rootProject.projectDir, relative)
}

fun projectPath(relative: String): String {
    return projectFile(relative).absolutePath
}

task<JavaExec>("start-mas") {
    sourceSets {
        main { 
            // runtimeClasspath.forEach { println(it) }
            classpath = runtimeClasspath
            args = listOf(
                project.property("mas2jFile").toString() + ".mas2j"
            )
        }
    }
    main = RunJasonProject::class.java.name
    standardInput = System.`in`
    
    doFirst {
        println("Setting-up Ant:")
        val config = Config.get()
        config.fix();
        config.setAntLib(projectPath("libs"))
        println("AntLib: '${config.getAntLib()}'")
        println("JavaHome: '${config.getJavaHome()}'")
        config.setProperty(Config.JASON_JAR, projectPath("libs/jason-2.4.jar"))
        println("JasonJar: '${config.getJasonJar()}'")
        config.store()
        println("\tStored new Config: ${config}")
        println("Running `java -cp ${classpath!!.joinToString(":")} ${main} ${args!!.joinToString(" ")}`...")
    }
}