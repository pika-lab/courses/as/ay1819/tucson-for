# TuCSoN for Jade/Jason – Exercises  

## Important remarks

* Always remember to run:

    ```bash
    ./gradlew --stop
    ```

    after each demo is completed, in order to ensure all TuCSoN nodes are actually shutdown.

* You can start a TuCSoN node by running:

    ```bash
    ./gradlew tucson -Pport=<node port>
    ```

* You can start a TuCSoN CLI by running:

      ```bash
      ./gradlew cli -Pport=<node port>
      ```

* You can start a TuCSoN Inspector by running:

    ```bash
    ./gradlew inspector
    ```

## Exercise 1 – TuCSoN4Jason (`jason` module)

* Start a TuCSoN node

    ```bash
    ./gradlew tucson
        ```

* Start a properly configured Jason MAS with GUI:

    ```bash
    ./gradlew start-mas
    ```

## Exercise 2 – TuCSoN4Jade (`jade` module)

* Start a properly configured Jade plat form with GUI:

    ```bash
    ./gradlew start-platform
    ```

* Add the Buyer and Seller agents to the platform's main container using Jade's GUI
